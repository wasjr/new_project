from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login

from .forms import NewForm, LoginForm
from .forms import NewForm

def register(request):
	message = None
	if request.method == 'POST':
		form = NewForm(request.POST)
		if form.is_valid:
			username = request.POST['username']
			password = request.POST['password']
			email = request.POST['email']
			first_name = request.POST['first_name']
			last_name = request.POST['last_name']
			message = 'Usuario {} cadastrado com sucesso!'.format(username) 
			User.objects.create_user(
				username=username,
				password=password,
				email=email,
				first_name=first_name,
				last_name=last_name,
				is_active=True,
				is_staff=True,
				is_superuser=True
				)

	form = NewForm()
	return render(request, 'register.html', {'form':form, 'success_message':message})



def login_user(request):
	form = LoginForm()
	message = None
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(request, username=username, password=password)
			if user:
				login(request, user)
				message = 'Usuario {} está logado.'.format(username)
	return render(request, 'login.html', {'form':form, 'success_message':message})

